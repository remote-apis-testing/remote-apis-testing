#!/usr/bin/env python3

"""Reads in the matrix.yml file and returns the DISPLAY_VERSION for
as associated client or server name."""

import argparse
import sys
from pathlib import Path
from create_docker_compose_yaml import get_matrix
from ruamel.yaml import YAML

DEFAULT_MATRIX_FILE = "../matrix.yml"


def main(args):
    """Reads in the matrix.yml file and returns the DISPLAY_VERSION for
    as associated client or server name."""
    matrix_yaml = get_matrix(args.matrix_path)

    try:
        print(
            matrix_yaml["projects"][args.search_name]["version_refs"][
                "DISPLAY_VERSION"
            ]["value"],
            file=sys.stdout,
        )
    except KeyError as e:
        print(f"{e}, did you spell this right? Or is it not in the matrix file?")


def get_args():
    """Get commandline arguments."""
    arg_parser = argparse.ArgumentParser(description=__doc__)

    arg_parser.add_argument(
        "-s",
        "--search_name",
        type=str,
        help=(
            "Name of the client or server which you require the display" "version for."
        ),
    )

    arg_parser.add_argument(
        "-m",
        "--matrix_file",
        default=DEFAULT_MATRIX_FILE,
        dest="matrix_path",
        type=Path,
        help=(
            "Path to the matrix file containing relevant display versions"
            f"Defaults to '{DEFAULT_MATRIX_FILE}'"
        ),
    )
    return arg_parser.parse_args()


if __name__ == "__main__":
    ARGS = get_args()
    main(ARGS)

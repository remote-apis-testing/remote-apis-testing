{
  fetcher: {
    http: {},
  },

  contentAddressableStorage: {
    grpc: {
      address: 'frontend:8980',
    },
  },

  assetCache: {
    actionCache: {
      grpc: {
        address: 'frontend:8980',
      },
    },
  },
  global: {
    diagnosticsHttpServer: {
      httpServers: [{
        listenAddresses: [':9980'],
        authenticationPolicy: { allow: {} },
      }],
      enablePrometheus: true,
      enablePprof: true,
      enableActiveSpans: true,
    },
  },
  grpcServers: [{
    listenAddresses: [':8979'],
    authenticationPolicy: { allow: {} },
  }],
  allowUpdatesForInstances: ['remote-execution'],
  maximumMessageSizeBytes: 16 * 1024 * 1024 * 1024,
  fetchAuthorizer: { allow: {} },
  pushAuthorizer: { allow: {} },
}

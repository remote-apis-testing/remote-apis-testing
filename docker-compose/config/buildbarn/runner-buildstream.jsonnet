{
  buildDirectoryPath: '/worker/build',
  grpcServers: [{
    listenPaths: ['/worker/runner-buildstream'],
    authenticationPolicy: { allow: {} },
  }],
  chrootIntoInputRoot: true,
}

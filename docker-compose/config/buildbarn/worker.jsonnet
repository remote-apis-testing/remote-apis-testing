// The contents of this file are covered by APACHE License Version 2.
// Modifications were made to this file for remote-apis-testing compatibility

local common = import 'common.libsonnet';

{
  blobstore: common.blobstore,
  browserUrl: common.browserUrl,
  maximumMessageSizeBytes: common.maximumMessageSizeBytes,
  scheduler: { address: 'scheduler:8983' },
  global: common.global,
  buildDirectories: [{
    native: {
      buildDirectoryPath: '/worker/build',
      cacheDirectoryPath: '/worker/cache',
      maximumCacheFileCount: 10000,
      maximumCacheSizeBytes: 1024 * 1024 * 1024,
      cacheReplacementPolicy: 'LEAST_RECENTLY_USED',
    },
    runners: [
      {
        endpoint: { address: 'unix:///worker/runner-buildstream' },
        concurrency: 8,
        instanceNamePrefix: 'remote-execution',
        platform: {
          properties: [
            { name: 'ISA', value: 'x86-64' },
            { name: 'OSFamily', value: 'linux' },
          ],
        },
        inputRootCharacterDeviceNodes: ['null', 'zero', 'random'],
      },
      {
        endpoint: { address: 'unix:///worker/runner' },
        concurrency: 8,
        instanceNamePrefix: 'remote-execution',
        platform: {
          properties: [
            { name: 'CHROOT', value: 'false' },
            { name: 'OSFamily', value: 'linux' },
          ],
        },
        environmentVariables: { 'PATH': '/usr/bin' },
      },
    ],
  }],
  inputDownloadConcurrency: 11,
  outputUploadConcurrency: 11,
  directoryCache: {
    maximumCount: 1000,
    maximumSizeBytes: 1000 * 1024,
    cacheReplacementPolicy: 'LEAST_RECENTLY_USED',
  },
}
